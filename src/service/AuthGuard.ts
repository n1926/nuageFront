import jwtDecode from 'jwt-decode';

export const isAuthenticated = () => {
  let token: string | null = localStorage.getItem('token');

  let decoded;

  if (!token) {
    return false;
  } else if (token) {
    decoded = jwtDecode(token);
    if (new Date() < new Date(decoded.exp * 1000)) {
      return true;
    }
  }
};
