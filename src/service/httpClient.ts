import axios from 'axios';

export const httpClient = axios.create({
  baseURL: `${process.env.REACT_APP_BASE_URL}/api/`,
  timeout: 60000,
});
httpClient.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

httpClient.interceptors.request.use(config => {
  const token = localStorage.getItem('token');

  if (token ) {
    // add header for Authorization
    if (config.headers) {
      config.headers.Authorization = `Bearer ${token}`;
      config.headers['Content-Type'] = 'application/json';
    }
  } else {
    config.headers && delete config.headers.Authorization;
  }

  return config;
});
