import { LOGIN_FAIL, LOGIN_SUCCESS } from './type';
import { httpClient } from '../../../../service/httpClient';

interface actionProps {
  username:string,
  password:string

}
export const login =
  ({ username, password }:actionProps) =>
  async dispatch => {
    try {
      const res = await httpClient.post('auth/login', { username, password });
      await localStorage.setItem('token', res.data.token);
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    } catch (e) {
      localStorage.removeItem('token');
      dispatch({ type: LOGIN_FAIL });
    }
  };
