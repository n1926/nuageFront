import { LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT } from './type';

interface authState {
  token: string | null;
  isAuthenticated: boolean | null;
  loading: boolean | null;
}
const initialState: authState = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  loading: false,
  // user: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        ...payload,
        isAuthenticated: true,
        loading: false,
      };
    case LOGIN_FAIL:

    case LOGOUT:
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
      };
    default:
      return state;
  }
}
