import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Button, Checkbox, Form, Input, Space, TextArea } from 'antd-mobile';
import { useState } from 'react';
import { Image } from 'antd';
import { useDispatch } from 'react-redux';
import { postRescue } from './formRescueSlice/action';

export function HomePage() {
  const [visitorName, setVisitorName] = useState('');
  const [rescueeName, setRescueeName] = useState('');
  const [last, setLast] = useState('');
  const [lastrescue, setLastrescue] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [message, setMessage] = useState('');
  const dispatch = useDispatch();

  const handleLogin = () => {
    dispatch(
      postRescue({
        visitorName: visitorName,
        visitorLastName: last,
        phoneNumber: phone,
        visitorEmail: email,
        rescueeName: rescueeName,
        rescueeLastName: lastrescue,
        message: message,
      }),
    );
  };

  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <div className="outer">
        <div className="inner">
          <Form
            onFinish={() => {
              handleLogin();
            }}
            footer={
              <Button block type="submit" color="primary">
                submit
              </Button>
            }
          >
            <h3>Rescue Form</h3>
            <Form.Item
              name=" Name "
              label=" Name "
              rules={[{ required: true, message: 'Username cannot be empty' }]}
            >
              <Input
                value={visitorName}
                onChange={val => setVisitorName(val)}
                placeholder=" Please enter your Name "
              />
            </Form.Item>
            <Form.Item
              name=" LastName "
              label=" Last Name "
              rules={[{ required: true, message: 'Last name cannot be empty' }]}
            >
              <Input
                value={last}
                onChange={val => setLast(val)}
                placeholder=" Please enter your last name "
              />
            </Form.Item>
            <Form.Item
              name=" Email "
              label=" Email "
              rules={[{ required: true, message: 'Email cannot be empty' }]}
            >
              <Input
                type={'email'}
                value={email}
                onChange={val => setEmail(val)}
                placeholder=" Please enter your email "
              />
            </Form.Item>
            <Form.Item
              name=" rescueName "
              label=" Rescuee Name "
              rules={[
                { required: true, message: 'Rescuee name cannot be empty' },
              ]}
            >
              <Input
                value={rescueeName}
                onChange={val => setRescueeName(val)}
                placeholder=" Please enter your Rescuee name "
              />
            </Form.Item>
            <Form.Item
              name=" rescueName "
              label=" Rescuee Last Name "
              rules={[
                {
                  required: true,
                  message: 'Rescuee Last name cannot be empty',
                },
              ]}
            >
              <Input
                value={lastrescue}
                onChange={val => setLastrescue(val)}
                placeholder=" Please enter your Rescuee Last name "
              />
            </Form.Item>
            <Form.Item
              name=" Phone "
              label=" Phone number "
              rules={[
                { required: true, message: 'Phone number cannot be empty' },
              ]}
            >
              <Input
                type={'tel'}
                value={phone}
                onChange={val => setPhone(val)}
                placeholder=" Please enter your phone number "
                pattern={'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$'}
              />
            </Form.Item>{' '}
            <Form.Item
              name=" message "
              label=" Message : "
              rules={[
                { required: true, message: 'Phone number cannot be empty' },
              ]}
            >
              <TextArea
                rows={4}
                value={message}
                onChange={val => setMessage(val)}
              />
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
}
