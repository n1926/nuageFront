import { POST_FORM_FAIL, POST_FORM_SUCCESS } from './type';

interface RescueeData {
  id: 0;
  visitorName: string;
  visitorLastName: string;
  phoneNumber: string;
  visitorEmail: string;
  rescueeName: string;
  rescueeLastName: string;
  dateNaissance: 'string';
  message: string;
  statusEnum: string;
}
const initialState: RescueeData[] |[]= [];

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case POST_FORM_SUCCESS:
      return [...state,payload];
    case POST_FORM_FAIL:

    default:
      return state;
  }
}
