import {POST_FORM_FAIL,POST_FORM_SUCCESS } from './type';
import { httpClient } from '../../../../service/httpClient';

interface actionProps {
  visitorName: string;
  visitorLastName: string;
  phoneNumber: string;
  visitorEmail: string;
  rescueeName: string;
  rescueeLastName: string;
  message: string;
}
export const postRescue = (props: actionProps) => async dispatch => {
  try {
    const res = await httpClient.post('post-rescue-event', { ...props });
    dispatch({
      type: POST_FORM_SUCCESS ,
      payload: res.data,
    });
  } catch (e) {
    localStorage.removeItem('token');
    dispatch({ type: POST_FORM_FAIL });
  }
};
