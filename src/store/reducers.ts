/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from '@reduxjs/toolkit';

import AuthReducer from '../app/pages/Auth/AuthSlice/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export const combined = combineReducers({
  AuthReducer: AuthReducer,
});
