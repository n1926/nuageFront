import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import { combined } from './reducers';

const initialState = {};
let middleware = [thunk, reduxLogger];

const composeEnhacer = compose(applyMiddleware(...middleware));
const store = createStore(combined, initialState, composeEnhacer);
export default store;
